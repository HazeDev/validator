<?php


$data = [
	'user_id' => 125,
	'email' => 'denisgmail.com',
	'name' => 'hello wordl'
];

require_once '../vendor/autoload.php';

$validator = new Haze\Validator(new Haze\Rules());

$validator->handle($data)->rules([
	'email' => [
		'type' => 'email',
		'size' => [5,10],
		'required' => true,
		'nullable' => false
	],
	'name' => [
		'type' => 'string',
		'required' => true,
		'nullable' => true
	]
]);

echo "<pre>";

print_r($validator->getErrors('email'));

if ($validator->isErrors()) {
	echo "есть ошибки";
}

echo $validator->getErrorsToJson('email');