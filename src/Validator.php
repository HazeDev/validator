<?php 

namespace Haze;

class Validator
{

	private $passable;

	private $rulesMethods;

	private $errors;

	public function __construct(RuleInterface $rulesMethods)
	{
		$this->rulesMethods = $rulesMethods;
	}

	public function handle($passable)
	{
		$this->passable = $passable;

		return $this;
	}

	public function rules(array $rules)
	{

		foreach ($rules as $name => $rules) {
			
			foreach ($rules as $nameValidateMethod => $rule) {
				
				$response = $this->rulesMethods->make(
					$this->passable[$name],
					$nameValidateMethod,
					$rule
				);

				if ($response !== true) {
					$this->addErrors($name, $response);
				} 	 
			}
		}

	}

	public function addErrors($name,$message)
	{
		$this->errors[$name][] = $message;
	}

	public function isErrors()
	{
		return !empty($this->errors);
	}

	public function getErrors($name = null)
    {
        if ($name) return $this->errors[$name];

        return $this->errors;
    }

    public function getErrorsToJson($name = null)
    {
        if ($name) return json_encode($this->errors[$name]);

        return json_encode($this->errors);
    }
}