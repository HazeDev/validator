<?php

namespace Haze;

class Rules implements RuleInterface
{
	private $errorMessage = [
		'email' => 'Email некорректен',
		'min_size' => 'поле меньше заданного заначение от',
		'max_size' => 'поле больше заданного заначение',
		'string' => 'это не строка',
		'integer' => 'не число',
		'nullable' => 'пораметр не может принимать значение null'
	];

	private function size($passable, array $rule)
	{
		$length = strlen($passable);

		$min = $rule[0];
		$max = $rule[1];

		if ($length < $min) {
			return $this->validateResponse(false,'min_size');
		} elseif($length > $max) {
			return $this->validateResponse(false,'max_size');
		}

		return true;
	}

	private function type($passable, $type)
	{
		$response = false;

		switch ($type) {
		    case 'email':
		        $response = $this->validateResponse(
		        	$this->validateEmail($passable),
		        	'email'
		        );
		        break;
		    case 'string':
		    	$response = $this->validateResponse(
		    		is_string($passable),
		    		'string'
		    	);
		    	break;
		    case 'integer':
		    	$response = $this->validateResponse(
		    		is_int($passable),
		    		'integer'
		    	);
		    	break;
		}

		return $response;
	}

	private function required($passable)
	{
		return true;
	}

	public function nullable($passable, bool $rule)
	{
		if (!$rule) {
			return $this->validateResponse(
				!is_null($passable),
				'nullable'
			);
		}

		return true;
		
	}

	public function make($passable, $method, $rule)
	{
		return $this->$method($passable,$rule);
	}


	private function validateResponse($passed, $errorName)
	{
		if (!$passed) return $this->errorMessage[$errorName];

		return true; 
	}

	private function validateEmail($email)
	{
		return preg_match('#^([\w]+\.?)+(?<!\.)@(?!\.)[a-zа-я0-9ё\.-]+\.?[a-zа-яё]{2,}$#ui', $email);
	}
}