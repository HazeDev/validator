<?php

namespace Haze;


interface RuleInterface
{
    public function make($passable, $method, $rule);
}